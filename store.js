import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
Vue.use(Vuex)
const store = new Vuex.Store({
		state: {
		},
		getters: {
		},
		mutations: {
			setMachine (state, payload) {
				state.Machine = payload
			},
		},
		plugins: [createPersistedState({
			storage: {
				getItem: key => uni.getStorageSync(key),
				setItem: (key, value) => uni.setStorageSync(key, value),
				removeItem: key => uni.removeStorageSync(key)
			},
		})]
})

export default store
