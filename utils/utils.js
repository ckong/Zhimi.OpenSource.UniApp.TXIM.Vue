const utils = {
    p (cb) {
        return new Promise((next, reject) => cb(next, reject))
    },
    delay (ms = 100) {
        return new Promise((next, reject) => setTimeout(next, ms))
    },
    query (self, selector) {
        return utils.p(next => {
            let query = uni.createSelectorQuery().in(self);
            query.select(selector)
                .fields({
                    rect: true,
                    size: true,
                    scrollOffset: true
                }, res => next(res))
                .exec();
        })
    },
    toast (str) {
        uni.showToast({ icon: 'none', position: 'bottom', title: str })
    }
}
export default utils
