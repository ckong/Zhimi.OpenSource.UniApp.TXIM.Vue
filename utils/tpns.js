var sdkwx = uni.requireNativePlugin('Zhimi-TPNS-local')
console.log(sdkwx)
const tpns = {
    cbMap: {  },
    on (event_type, cb) {
        tpns.cbMap[event_type] = tpns.cbMap[event_type] || []
        tpns.cbMap[event_type].push(cb)
    },
    off (event_type, cb) {
        tpns.cbMap[event_type] = tpns.cbMap[event_type] || []
        let index = tpns.cbMap[event_type].indexOf(cb)
        if (index) {
            tpns.cbMap[event_type].splice(index, 1)
        }
    },
    start () {
      sdkwx.startTPNS({
          ios:{
              accessId:1680004241,
              accessKey:""
          },
          xiaomi:{//设置小米
              appId:"2882303761518945852",
              appKey:"5251894514852"
          },
          oppo:{//设置oppo
              appKey:"5ea511af3be74ddaa9af587102d8c5e3",
              appSecret:"e7d79bae797c4b379e02a49fd97081fe"
          },
          meizu:{//设置魅族
              appId:"",
              appKey:""
          }
      }, function(res) {
          console.log(res);
          //事件回调处理
          let cbs = tpns.cbMap[res.type]
          if (cbs) {
              cbs.forEach(cb => cb(res))
          }
      })
    },
    stop () {
      sdkwx.stopTPNS(true)
    },
    getToken () {
      //腾讯的token
      var token = sdkwx.getToken();
      console.log("token:", token)

      //第三方的token
      var pushToken = sdkwx.getOtherPushToken();
      console.log("pushToken:", pushToken)
      return token || pushToken
    }
}

export default tpns