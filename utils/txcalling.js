const TRTCCalling = uni.requireNativePlugin('Zhimi-TRTCCalling');
// import TRTCCalling from '../tx-jssdk/trtc-calling-jssdk'
const SDK = {
    _p: (cb) => new Promise(cb),
    _eventMap: {  },
    // 获取实例 - 虚构方法
    sharedInstance () {
        return im
    },
		_inited: false,
    // 初始化SDK
    _init () {
        if (SDK._inited) {
            return
        }
        TRTCCalling.initJSSDK()
        SDK._inited = true
        let _cb = res => {
					// console.log(res)
          if (SDK._eventMap[res.type]) {
            SDK._eventMap[res.type].forEach(cb => {
                setTimeout(() => cb(res), 0)
            })
        }
        }
        TRTCCalling.addDelegate(_cb)
        SDK.$on('onInvited', async res => {
          console.log('收到要求')
          console.log(res)
          console.log('收到要求')
          let pages = getCurrentPages();
          let page = pages[pages.length - 1];
          if (page.route.indexOf('inline/calling') >= 0) {
						SDK.reject()
            return
					}
          try {
            await SDK.checkPermissions()
            console.log('收到邀请')
            console.log(res)
            uni.navigateTo({
              url: `/pages/inline/calling?sender=false&receivers=[]&type=${['','audio','video'][res.data.type]}`
            })
          } catch (e) {
            SDK.reject()
          }
        })
    },
    // 销毁函数，如果无需再运行该实例，请调用该接口。
    destroy () {
        TRTCCalling.destroy()
    },
    // 添加全局事件监听
    $on (eventType, callback) {
        SDK._eventMap[eventType] = SDK._eventMap[eventType] || []
        SDK._eventMap[eventType].push(callback)
    },
    // 删除全局事件监听
    $off (eventType, callback) {
        SDK._eventMap[eventType] = SDK._eventMap[eventType] || []
        let index = SDK._eventMap[eventType].indexOf(callback)
        if (index >= 0) {
            SDK._eventMap[eventType].splice(index, 1)
        }
    },
    login (sdkAppId, userId, userSign, callback) {
      return SDK._p(next => {
        TRTCCalling.login(sdkAppId, userId + '', userSign, callback || next)
      })
    },
		logout () {
		  TRTCCalling.logout()
		},
    checkPermissions () {
      return SDK._p(next => {
        TRTCCalling.checkPermissions(({code}) => {
					console.log(code)
          !code && next()
        })
      })
    },
    // 单人通话邀请，当前处于通话中也可继续调用邀请他人。
    call (userId, type) {
			console.log({ userId, type })
      TRTCCalling.call(userId, type)
    },
    // IM 群组邀请通话，被邀请方会收到 onInvited() 回调。如果当前处于通话中，可以继续调用该函数继续邀请他人进入通话，同时正在通话的用户会收到 onGroupCallInviteeListUpdate() 回调。
    groupCall (userIdList, type, groupId) {
      console.log({ userIdList, type, groupId })
      TRTCCalling.groupCall(userIdList, type, groupId)
    },
    // 接受当前通话。当您作为被邀请方收到 onInvited() 的回调时，可以调用该函数接听来电。
    accept () {
      TRTCCalling.accept()
    },
    // 拒绝当前通话。当您作为被邀请方收到 onInvited() 的回调时，可以调用该函数拒绝来电。
    reject () {
      TRTCCalling.reject()
    },
    // 挂断当前通话。当您处于通话中，可以调用该函数结束通话。
    hangup () {
      TRTCCalling.hangup()
    },
		// setMicMute
		setMicMute (val) {
			TRTCCalling.setMicMute(val)
		},
		// switchCamera
		switchCamera (val) {
			TRTCCalling.switchCamera(val)
		},
		// setHandsFree
		setHandsFree (val) {
			TRTCCalling.setHandsFree(val)
		},
    openCamera (isFrontCamera, view) {
        TRTCCalling.openCamera(isFrontCamera, view)
    },
    startRemoteView (userId, view) {
        TRTCCalling.startRemoteView (userId, view)
    },
    stopRemoteView (userId, view) {
        TRTCCalling.stopRemoteView (userId, view)
    },
    closeCamera () {
        TRTCCalling.closeCamera()
    }
}
SDK._init()
export default SDK
