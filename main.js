import 'datejs'
import Vue from 'vue'
import App from './App'
import store from './store'

// Components
import ChatContactItem from './components/ChatContactItem'
import ChatMessageItem from './components/ChatMessageItem'
import ChatLayout from './components/ChatLayout'
import ChatInputBox from './components/ChatInputBox'

Vue.component('ChatContactItem', ChatContactItem)
Vue.component('ChatMessageItem', ChatMessageItem)
Vue.component('ChatLayout', ChatLayout)
Vue.component('ChatInputBox', ChatInputBox)

// Utils
import utils from './utils/utils'
import imUtils from './utils/im'
import txim from './utils/txim'
// import txcalling from './utils/txcalling'
// import tpns from './utils/tpns'

txim.initSDK(txim.sdkAppId)

Vue.prototype.$utils = utils
Vue.prototype.$imUtils = imUtils
Vue.prototype.$txim = txim
// Vue.prototype.$txcalling = txcalling
// Vue.prototype.$tpns = tpns
// tpns.start()
// console.log(tpns.getToken())
Vue.prototype._ = _
Vue.prototype.$next = function () {
    return new Promise(next => this.$nextTick(next))
}
Vue.prototype.$delay = function (ms) {
    return new Promise(next => setTimeout(next, ms))
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App,
    store
})
app.$mount()
