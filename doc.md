# 智密 - 腾讯云即时通讯(TXIM)

#### 介绍
智密-腾讯云即时通讯(TXIM)是一个uniapp的第三方原生插件，以最大api还原度的方式集成腾讯云即时通讯TXIM SDK，并且提供对应的jssdk，以便开发者随心所欲的调用TXIM SDK，使得原生插件与官方文档统一，降低开发难度，提升开发效率。

#### 应用截图
| 1 | 2 | 3 |
| ---- | ---- | ---- |
| ![](https://files.qiadoo.com/CareBoBo/Common/2021/05/23/1d0a21f7-55c2-4a16-9cbc-b429339b0b63.png) | ![](https://files.qiadoo.com/CareBoBo/Common/2021/05/23/0675f95f-282b-48e2-8266-f0f13775577d.jpeg) | ![](https://files.qiadoo.com/CareBoBo/Common/2021/05/23/a9e77363-166f-4eac-b5ba-97519712a10c.jpeg) |

#### 应用场景
智密-腾讯云即时通讯(后续简称TXIM)支持多种应用场景，包括但不限于以下场景
- 社交应用
- 店铺客服
- 嵌入式社交模块
- 在线直播通讯
...


#### 业务介绍
单聊
单聊即 1V1 聊天，提供包括文字、表情、地理位置、图片、语音、短视频及自定义消息的能力，可实现红包、对话机器人、消息回执、消息撤回等特殊功能，除此之外还提供离线消息、漫游消息等服务。详细可参阅腾讯官方文档。
群聊
多人聊天服务，根据群组加群方式及管理组织形式的部分预设以下四种群组类型，可以适应各种群聊场景需求。
• 好友工作群（Work）：类似普通微信群，创建后仅支持已在群内的好友邀请加群，且无需被邀请方同意或群主审批。
• 陌生人社交群（Public）：类似 QQ 群，创建后群主可以指定群管理员，用户搜索群 ID 发起加群申请后，需要群主或管理员审批通过才能入群。
• 临时会议群（Meeting）：创建后可以随意进出，且支持查看入群前消息；适合用于音视频会议场景、在线教育场景等与实时音视频产品结合的场景。
• 直播群（AVChatRoom）：创建后可以随意进出，没有群成员数量上限，但不支持历史消息存储；适合与直播产品结合，用于弹幕聊天场景。
群组具备高度可定制性，包括自定义群组类型、自定义群组字段、自定义群成员字段、自定义群组 ID、自定义事件回调等。App 可以根据自己的需求进行深度定制。详细可参阅腾讯官方文档。

#### 插件说明文档
开放基本对象如下：
````
let sdkwx = uni.requireNativePlugin("TX-IM");
let message = uni.requireNativePlugin("TXIM-Message");
let conversation = uni.requireNativePlugin("TXIM-Conversation");
let group = uni.requireNativePlugin("TXIM-Group");
let friendship = uni.requireNativePlugin("TXIM-Friendship");
let sdkAppId = 1400486110
let sdkConfig = { logLevel: 0 }
sdkwx.initSDK(sdkAppID, sdkConfig, res => {
  // 此处res为 TXIM SDK输出回调信息
})
````
为保证插件的说明文档可以实时更新以及更好的描述插件所需的配置等信息，
插件以及Demo的详细介绍请查阅：https://www.yuque.com/zhimikeji/rggqn1/macpzt

#### 插件说明
插件可免费使用，但是需要开发者捆绑腾讯云

1.企业账号与我们关联 可以无限制使用我们的一个腾讯系插件 第二个插件开始就有消费要求
2.个人账号绑定的 第一个账号就有5000/年的消费保障 无法消费到的 需要按照插件定价支付价格

#### 其他
杭州智密科技有限公司专业定制各类Uni-app插件，承接基于Uni-app的APP项目外包； 公司有专职硬件工程师，对IC/ID读卡器、摄像头、身份证读卡器、RFID、打印机、指纹识别、门锁等各类硬件SDK集成经验丰富(也可提供一体机定制服务,承接软硬件一体的交钥匙项目)。 插件有任何问题请高抬贵手不要随意打低分，请联系微信:zhimitec，提交bug，我们会尽快修复。
